# Breaktrough

Simple game written in C using [Raylib](https://github.com/raysan5/raylib)

## Compiling and running

In libs/ put libraylib.a, either by compiling raylib yourself (5.0 version) or by grabbing a precompiled binary from [here](https://github.com/raysan5/raylib/releases/tag/5.0)

```console
$ ./build.sh
$ ./breaktrough
```

## Optional command line arguments

| Argument | Explanation |
| :---: | :--- |
| `--show-fps` | Displays an fps counter in the top-left corner |
| `--show-directions` | Displays debug lines indicating entities' direction |
| `--multiplayer` | Switch to 2 players multiplayer mode |
| `--duck` | Duck |
