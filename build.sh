#!/bin/sh

set -xe

CC=gcc
CFLAGS="-Wall -Wextra -Wpedantic -std=c17 -Os"
LIBS="-lraylib -lm"
SRC="src/game.c"
OUT="breaktrough"

${CC} ${CFLAGS} -Llibs -o ${OUT} ${SRC} ${LIBS}
