// @todo destroy balls when they hit the floor
// @todo if fully inside, move the rect outside the collider
// @todo pause the game when pressing a button

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "raylib.h"
#include "raymath.h"

#ifndef M_PI
#define M_PI 3.141592653f
#endif

#define ARRAY_LEN(xs) (sizeof((xs))/sizeof((*xs)))

#define WINDOW_FACTOR 200
#define WINDOW_WIDTH (4 * WINDOW_FACTOR)
#define WINDOW_HEIGHT (3 * WINDOW_FACTOR)

#define DEBUG_ARROWS_START_RADIUS (WINDOW_FACTOR / 50)
#define DEBUG_ARROWS_WIDTH (WINDOW_FACTOR / 60)
#define DEBUG_ARROWS_LENGTH_FACTOR (WINDOW_FACTOR / 5)

#define BALLS_NUMBER 1 // h-how many balls do y-you have??

#define BALL_MAGNITUDE (WINDOW_FACTOR * 1)

#define PLAYER_WIDTH (3 * WINDOW_FACTOR / 4)
#define PLAYER_HEIGHT (WINDOW_FACTOR / 8)
#define PLAYER_MAGNITUDE (WINDOW_FACTOR * 5)

#define BLOCK_WIDTH 100
#define BLOCK_HEIGHT 50

#define CELLS_COLUMNS 7
#define CELLS_ROWS 5

#define CELLS_SCREEN_PADDING (4 * WINDOW_FACTOR / 20)

enum Direction {
	DIRECTION_LEFT = 0,
	DIRECTION_RIGHT,

	DIRECTION_COUNT
};

static KeyboardKey get_key_binding(size_t player_index, enum Direction dir) {
	assert(player_index < 2);
	static const KeyboardKey player_bindings[][DIRECTION_COUNT] = {
		[0] = { KEY_A, KEY_D },
		[1] = { KEY_LEFT, KEY_RIGHT },
	};
	assert(dir >= 0 && dir < DIRECTION_COUNT);

	return player_bindings[player_index][dir];
}

typedef struct {
	Rectangle rect;
	Vector2 direction;
} Collider;

static inline float random_float(void) {
	return (float)rand() / RAND_MAX;
}

// clamps between PI/4 and 3*pi/4 (+pi if the angle points down)
// doing so the ball doesn't have very horizontal angles, which make the game boring
static inline float clamp_to_fun_angle(float angle) {
	float v = (angle > M_PI) ? M_PI : 0;
	return Clamp(angle, M_PI/4 + v, 3*M_PI/4 + v);
}

static inline Collider generate_random_ball(Rectangle region) {
	float angle = clamp_to_fun_angle(6 * M_PI / 4 + random_float() * (M_PI / 2));

	Collider b = {
		.direction = {
			.x = cos(angle),
			.y = sin(angle),
		}

	};
	b.rect.width = 3.0 * WINDOW_FACTOR / 5.0;
	b.rect.height = 2.0 * WINDOW_FACTOR / 5.0;
	b.rect.x = (float)GetRandomValue(region.x, region.x + region.width - b.rect.width);
	b.rect.y = (float)GetRandomValue(region.y, region.y + region.height - b.rect.height);
	return b;
}

// hack so the compiler doesn't complain that the colors are not constant
// (they are defined as compound literals, so lvalues that can't be read in constant expressions)
#define CLITERAL(...)
static const Color colors[] = {
	PINK, RED, MAROON,
	YELLOW, GOLD, ORANGE,
	PURPLE, VIOLET, DARKPURPLE,
	LIGHTGRAY, GRAY, DARKGRAY,
	GREEN, LIME, DARKGREEN,
	SKYBLUE, BLUE, DARKBLUE,
	BEIGE, BROWN, DARKBROWN,
};
#define CLITERAL(type) (type)

// when x >= 0 returns 1, otherwise -1
static inline int get_signf(float x) {
	return (x >= 0) - (x < 0);
}

static inline float get_angle(Vector2 v) {
	return acosf(v.x) * get_signf(v.y);
}

static bool handle_collider_rect_collision(Rectangle *next_ball_rect, Collider *ball, Collider collider) {
	//if (collider.width == 0 || collider.height == 0) return false;
	Rectangle collision = GetCollisionRec(*next_ball_rect, collider.rect);
	if (collision.width == 0 || collision.height == 0) return false;

	if (collision.width > collision.height) {
		ball->direction.y *= -1;

		if (collision.y == collider.rect.y) // hit from the top
			next_ball_rect->y = collision.y - next_ball_rect->height;
		else if (collision.y + collision.height == collider.rect.y + collider.rect.height) // hit from the bottom
			next_ball_rect->y = collision.y + collision.height;
		else { // @todo if fully inside, move the rect outside the collider
		}
	} else {
		ball->direction.x *= -1;

		if (collision.x == collider.rect.x) // hit from the left
			next_ball_rect->x = collision.x - next_ball_rect->width;
		else if (collision.x + collision.width == collider.rect.x + collider.rect.width) // hit from the right
			next_ball_rect->x = collision.x + collision.width;
		else { // @todo if fully inside, move the rect outside the collider
		}
	}

	if (collider.direction.x != 0 || collider.direction.y != 0) {
		float ball_angle = get_angle(ball->direction);
		float collider_angle = get_angle(collider.direction);
		float variation = get_signf(ball_angle) * Clamp(collider_angle * 0.3, 0, 0.2);
		float new_angle = Wrap(ball_angle + variation, 0, 2*M_PI);

		new_angle = clamp_to_fun_angle(new_angle);
		ball->direction.x = cos(new_angle);
		ball->direction.y = sin(new_angle);
	}

	return true;
}

static Collider update_ball(Collider ball, float dt, Collider *collider_rects, size_t collider_rects_len) {
	Rectangle next_ball_rect = {
		.x      = ball.rect.x + ball.direction.x * BALL_MAGNITUDE * dt,
		.y      = ball.rect.y + ball.direction.y * BALL_MAGNITUDE * dt,
		.width  = ball.rect.width,
		.height = ball.rect.height,
	};
	// colliders
	for (size_t i = 0; i < collider_rects_len; ++i) {
		if (!handle_collider_rect_collision(&next_ball_rect, &ball, collider_rects[i]) || i == 0) continue;

		collider_rects[i].rect.width = 0;
	}

	// screen border
	{
		float window_width = GetRenderWidth();
		float window_height = GetRenderHeight();
		if (next_ball_rect.x < 0 || next_ball_rect.x + next_ball_rect.width > window_width) {
			ball.direction.x *= -1;
			next_ball_rect.x = ball.rect.x;
		}
		if (next_ball_rect.y < 0 || next_ball_rect.y + next_ball_rect.height > window_height) {
			ball.direction.y *= -1;
			next_ball_rect.y = ball.rect.y;
		}
	}
	ball.rect.x = next_ball_rect.x;
	ball.rect.y = next_ball_rect.y;

	return ball;
}

static inline const char *next_cli_arg(int *argc, const char ***argv) {
	(*argc)--;
	return *((*argv)++);
}

static inline void program_usage(const char *program_name) {
	fprintf(stderr, "USAGE: %s [OPTIONS..]\n", program_name);
	fprintf(stderr, "  OPTIONS:\n");
	fprintf(stderr, "    --show-fps        |  Displays an fps counter in the top-left corner\n");
	fprintf(stderr, "    --show-directions |  Displays debug lines indicating entities' direction\n");
	fprintf(stderr, "    --multiplayer     |  Switch to 2 players multiplayer mode\n");
	fprintf(stderr, "    --duck            |  Duck\n");
	fprintf(stderr, "\n");
}

void render_collider_direction(Collider coll, Color color) {
	Vector2 center = {
		.x = coll.rect.x + coll.rect.width / 2,
		.y = coll.rect.y + coll.rect.height / 2,
	};
	DrawCircleV(center, DEBUG_ARROWS_START_RADIUS, color);
	DrawLineEx(center, Vector2Add(center, Vector2Scale(coll.direction, DEBUG_ARROWS_LENGTH_FACTOR)), DEBUG_ARROWS_WIDTH, color);
}

int main(int argc, const char **argv) {
	const char *program_name = next_cli_arg(&argc, &argv);

	bool show_fps = false;
	bool show_directions = false;
	bool duck = false;
	bool multiplayer = false;

	while (argc > 0) {
		const char *arg = next_cli_arg(&argc, &argv);

		if (strcmp(arg, "--show-fps") == 0) {
			show_fps = true;
		} else if (strcmp(arg, "--show-directions") == 0) {
			show_directions = true;
		} else if (strcmp(arg, "--duck") == 0) {
			duck = true;
		} else if (strcmp(arg, "--multiplayer") == 0) {
			multiplayer = true;
		} else {
			fprintf(stderr, "Couldn't recognize '%s' as a known command line argument\n", arg);
			program_usage(program_name);
		}
	}

	SetTraceLogLevel(LOG_WARNING);

	srand(time(NULL));
	SetRandomSeed(time(NULL));

	Vector2 window_size = {
		.x = WINDOW_WIDTH,
		.y = WINDOW_HEIGHT
	};

	Collider players[2] = {
		{
			.rect = {
				.x = window_size.x/2 - PLAYER_WIDTH/2,
				.y = 7*window_size.y/8 - PLAYER_HEIGHT/2,
				.width = PLAYER_WIDTH,
				.height = PLAYER_HEIGHT,
			},
			.direction = {0},
		},
		{
			.rect = {
				.x = window_size.x/2 - PLAYER_WIDTH/2,
				.y = window_size.y/6 - PLAYER_HEIGHT/2,
				.width = PLAYER_WIDTH,
				.height = PLAYER_HEIGHT,
			},
			.direction = {0},
		}
	};
	size_t players_len = (multiplayer) ? ARRAY_LEN(players) : 1;

	Collider balls[BALLS_NUMBER] = {0};

	Rectangle balls_generation_zone = {
		.x = window_size.x/3,
		.y = window_size.y/3,
		.width = window_size.x/3,
		.height = window_size.y/3,
	};
	for (size_t i = 0; i < BALLS_NUMBER; ++i) {
		balls[i] = generate_random_ball(balls_generation_zone);
	}


	Collider collider_recs[ARRAY_LEN(players) + CELLS_COLUMNS * CELLS_ROWS] = {0};

	if (!multiplayer) {
		Rectangle cells_zone = {
			.x = CELLS_SCREEN_PADDING / 2,
			.y = CELLS_SCREEN_PADDING / 2,
			.width = window_size.x - CELLS_SCREEN_PADDING,
			.height = (window_size.y - CELLS_SCREEN_PADDING) / 3,
		};

		float cell_width = cells_zone.width / CELLS_COLUMNS;
		float cell_height = cells_zone.height / CELLS_ROWS;

		float cells_padding_horizontal = cell_width / 5;
		float cells_padding_vertical = cell_height / 5;
		for (size_t cy = 0; cy < CELLS_ROWS; ++cy) {
			for (size_t cx = 0; cx < CELLS_COLUMNS; ++cx) {
				Collider cell = {
					.rect = {
						.x = cells_zone.x + cx * cell_width + cells_padding_horizontal / 2,
						.y = cells_zone.y + cy * cell_height + cells_padding_vertical / 2,
						.width = cell_width - cells_padding_horizontal,
						.height = cell_height - cells_padding_vertical,
					},
					.direction = {0},
				};
				collider_recs[ARRAY_LEN(players) + cy * CELLS_COLUMNS + cx] = cell;
			}
		}
	}

	Image duck_img = {0};
	Texture2D duck_tex = {0};

	SetWindowState(FLAG_VSYNC_HINT);
	SetTargetFPS(60);
	InitWindow(window_size.x, window_size.y, "Breaktrough");

	if (duck) {
		duck_img = LoadImage("media/duck.png");
		while (!IsImageReady(duck_img)) WaitTime(0.1);
		ImageResizeNN(&duck_img, balls[0].rect.width, balls[0].rect.height);
		duck_tex = LoadTextureFromImage(duck_img);
		while (!IsTextureReady(duck_tex)) WaitTime(0.1);
	}

	while (!WindowShouldClose()) {
		float dt = GetFrameTime();
		int fps = GetFPS();

		if (IsKeyPressed(KEY_Q)) break;

		for (size_t i = 0; i < players_len; ++i) {
			players[i].direction.x = 0.0;
			if (IsKeyDown(get_key_binding(i, DIRECTION_LEFT))) {
				players[i].direction.x = -1.0;
			}
			if (IsKeyDown(get_key_binding(i, DIRECTION_RIGHT))) {
				players[i].direction.x = 1.0;
			}

			players[i].rect.x += PLAYER_MAGNITUDE * players[i].direction.x * dt;

			collider_recs[i] = players[i];
		}

		// balls collisions
		for (size_t i = 0; i < BALLS_NUMBER; ++i) {
			balls[i] = update_ball(balls[i], dt, collider_recs, ARRAY_LEN(collider_recs));
		}

		ClearBackground(BLACK);
		BeginDrawing();

		DrawRectangleRec(players[0].rect, GREEN);
		if (show_directions) render_collider_direction(players[0], GOLD);
		if (multiplayer) {
			DrawRectangleRec(players[1].rect, BLUE);
			if (show_directions) render_collider_direction(players[1], GOLD);
		}

		for (size_t i = ARRAY_LEN(players); i < ARRAY_LEN(collider_recs); ++i)
			DrawRectangleRec(collider_recs[i].rect, BLUE);

		for (size_t i = 0; i < BALLS_NUMBER; ++i) {
			Color tint = colors[i % ARRAY_LEN(colors)];
			if (duck) {
				DrawRectangleRec(balls[i].rect, ColorBrightness(tint, -0.9));
				DrawTexture(duck_tex, balls[i].rect.x, balls[i].rect.y, tint);
			} else {
				DrawRectangleRec(balls[i].rect, tint);
			}

			if (show_directions) render_collider_direction(balls[i], RED);

		}

		if (show_fps) DrawText(TextFormat("%-3d FPS", fps), 0, 0, 1 << 5, GREEN);

		EndDrawing();
	}

	if (duck) UnloadImage(duck_img);

	CloseWindow();

	return 0;
}
